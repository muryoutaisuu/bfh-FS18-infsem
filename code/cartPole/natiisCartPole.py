# https://gym.openai.com/docs/

import gym, sys
env = gym.make('CartPole-v0')
for i_episode in range(1):
    observation = env.reset()
    done = False
    for t in range(1000):
        env.render()
        # print(observation)
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        print(f"observation={observation}, reward={reward}, done={done}, info={info}")
        if done:
            print(f"Episode finished after {t+1} timesteps, reward={reward}")
            env.reset()
            break
sys.exit(0)